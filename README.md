
# Markinson Assessment - Aashir

  

Hi! For building this application you need to have nodejs installed in your machine if you don't please visit https://nodejs.org/en/download/ .

Make sure you have **bower** and **gulp** installed globally if not

**npm install -g bower**

**npm install -g gulp**

This application is component-based AngularJS v1 client app consuming the given api and performing the given tasks developed with npm and gulp.

  

# Features

* Retrieving the source data from the API using AngularJS services.

* Gulp tasks for watching SCSS, JS and HTML files

* Production Gulp build task

* Clear folder structure

* QueryService $http wrapper to handle REST API requests

* Identify the five most common words within the Company field

* Five most commonly occurring words with their count and corresponding top 5 company names .

  

## 1. Setup

  Go to root directory of the application

```bash

npm install

```

## 2. Make it run

```bash

gulp

```

- all SCSS/HTML/JS will be watched for changes and lint errors with browser reloading

# Files Structure

  

> /package.json

> /bower.json

> /gulpfile.js

Contains the dependencies and configuration information

> /styles contains stylesheets for the application

  

## src directory

  

- assets directory contains the images

- config directory contains the **config.js** file which stores all the **constants** required in the application.

- index.html contains all the includes required in application.

-  **app** directory contains **app.js** file which is the definition of main app module and its dependencies.

-  **app-routes.js** which contains the angular ui router information right now we have only one route.

- we have two components with their own controller and styles file ``navbar component`` and ``home component``

- home component contains all the logic

  

## Finding Most Common Words

  

You can find function in ``src\app\home\home.component.js`` named **nthMostCommon** which takes two inputs as arguments the string from which you find most occurring words and the amount is the top number of words you want to find in our case its 5.

Function uses the linear search and maintains the wordOccurences array and split the string into words, then loop through the words and increment a counter for each one.

  

## HomeCtrl

  

You can find function in ``src\app\home\home.component.js`` named **HomeCtrl** which calls the service **QueryService** and sends the get request with $http.get to the given url and retrieves data in the form of JSON. The code traverse through the whole response and concat all the company names in the data in the form of one string and sends that string to nthMostCommon function to find 5 most occuring words and also maintains all the unique company names in uniqueAllComapnyNames array and binds it with $scope to for two way data binding with view.

  

## The View

  

View uses bootstrap and css styles to render Navbar and card view.

Cardview contains **ng-repeat** in it which renders the card according to the size of array binds with it which is words. For every word a card is rendered with the word as h3 and count with a button which shows the companies containing that word in bootstrap dropdown ( The dropdown contains another ng-repeat to filter the names to 5.)

  

## Filters

  

Custom Filters used with ng-repeat with OrderBy pipe having an array ``['occurrences', 'word']``

  
  

## Application Process Flow

  
  
  

```mermaid

graph LR

A[View] --> B((HomeComponent))

B -- $http.get--> C(QueryService)

C --ReturnsData --> B(HomeComponent)

B --> D(Concating Company names in a single string)

D-->E(nthMostCommon)

E-->A

A --> F[ngrepeat]

F-- orderBy filter--> G(Card View)

  

```