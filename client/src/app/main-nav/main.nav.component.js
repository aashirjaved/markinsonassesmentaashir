;
(function() {

    'use strict';

    /**
     * Main navigation
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('aashirMarkinsonAssesment')
        .component('mainNav', {
            templateUrl: 'app/main-nav/main-nav.html',
            controllerAs: 'vm',
            controller: MainNavCtrl
        });

    MainNavCtrl.$inject = ['$scope'];

    /// definition

    function MainNavCtrl($scope, localStorage) {
        var vm = this;


    }

})();