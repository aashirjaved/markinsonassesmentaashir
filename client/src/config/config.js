;
(function() {


    /**
     * Place to store API URL or any other CONSTANTS
     * Usage:
     *
     * Inject CONSTANTS service as a dependency and then use like this:
     * CONSTANTS.API_URL
     */
    angular
        .module('aashirMarkinsonAssesment')
        .constant('CONSTANTS', {
            API: {
                dev: {
                    'API_URL': 'http://profiler.markinson.com.au/api/'
                },
                prod: {
                    'API_URL': 'http://profiler.markinson.com.au/api/'
                },
            }
        });


})();